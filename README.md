# baselines

## CPU + Dram watts consumption estimate.
* Use [line 60](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-60) of [line 62](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-62) in [watts\_estimate.py](https://bitbucket.org/sunybingcloud/task-profiling/src/master/watts_estimate.py).
* Use [line 187](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-187) instead of [line 188](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-188) in [watts\_estimate.py](https://bitbucket.org/sunybingcloud/task-profiling/src/master/watts_estimate.py).

### Medians of Medians of Max Peak Power consumption.

* Call function `max_peak_median(...)` in [line 190](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-190).

### Medians of Medians of Median Peak Power consumption.

* Call function `median_peak_median(...)` in [line 190](https://bitbucket.org/sunybingcloud/task-profiling/src/2a9198fbcebcd3e09286b3b62f072e863509651d/watts_estimate.py#lines-190).


## Run the following command to generate

```commandline
python3 watts_estimate.py /path/to/pcpdatadir /path/to/destdir <host>
```
