Median peak for Run 1 : 111.65290335919045
Median peak for Run 7 : 111.59722900390625
Median peak for Run 5 : 111.66499814188586
Median peak for Run 9 : 111.60240650849254
Median peak for Run 6 : 111.5930767850231
Median peak for Run 8 : 111.59886468814662
Median peak for Run 3 : 111.64773484379752
Median peak for Run 2 : 111.64369761104228
Median peak for Run 4 : 111.62685750833131
Median peak for Run 10 : 111.64771401466056

Median static power : 16.320655221760738
Median of all the median peaks (- static power) for phoronix_cryptography : 95.31462233792607