Median peak for Run 1 : 96.10238066481904
Median peak for Run 7 : 96.57770439202758
Median peak for Run 5 : 96.27320027462369
Median peak for Run 9 : 96.45933478816063
Median peak for Run 6 : 96.33156501830894
Median peak for Run 8 : 96.2984076681179
Median peak for Run 3 : 96.37748639808433
Median peak for Run 2 : 96.40709161716758
Median peak for Run 4 : 96.00630490641902
Median peak for Run 10 : 96.37476550343726

Median static power : 16.320655221760738
Median of all the median peaks (- static power) for dgemm_electron1 : 80.03251003911237