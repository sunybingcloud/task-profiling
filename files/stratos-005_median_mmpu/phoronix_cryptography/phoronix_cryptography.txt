Median peak for Run 1 : 169.853914436004
Median peak for Run 7 : 169.79067932729805
Median peak for Run 5 : 169.76008025296406
Median peak for Run 9 : 169.86116224970726
Median peak for Run 6 : 169.84462048314685
Median peak for Run 8 : 169.68945007879483
Median peak for Run 3 : 169.78232200119854
Median peak for Run 2 : 169.78571998744724
Median peak for Run 4 : 169.71399592581193
Median peak for Run 10 : 169.74270729563267

Median static power : 22.109637825723503
Median of all the median peaks (- static power) for phoronix_cryptography : 147.67438316859938