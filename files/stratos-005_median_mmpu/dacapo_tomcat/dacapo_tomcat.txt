Median peak for Run 1 : 58.74270236354647
Median peak for Run 7 : 61.90040619949903
Median peak for Run 5 : 67.02568460843199
Median peak for Run 9 : 64.13754222787824
Median peak for Run 6 : 58.40413758052257
Median peak for Run 8 : 70.1722129315252
Median peak for Run 3 : 68.40547474108637
Median peak for Run 2 : 65.73655500638066
Median peak for Run 4 : 70.07533540738305
Median peak for Run 10 : 63.18968141715624

Median static power : 22.109637825723503
Median of all the median peaks (- static power) for dacapo_tomcat : 42.827410791405946