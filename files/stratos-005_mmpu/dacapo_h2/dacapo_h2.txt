Max peak for Run 1 : 102.83596434084069
Max peak for Run 2 : 101.92625806415873
Max peak for Run 3 : 102.74754910605841
Max peak for Run 4 : 102.62874161551706
Max peak for Run 5 : 105.39022127445926
Max peak for Run 6 : 102.29556538830582
Max peak for Run 7 : 103.76822068463359
Max peak for Run 8 : 102.92515442852117
Max peak for Run 9 : 104.35130292843445
Max peak for Run 10 : 102.58275359379337

Median static power : 29.173311071
Median of all the max peaks (- static power) for dacapo:h2 : 73.6184456524