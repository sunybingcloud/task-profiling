Median peak for Run 1 : 35.0241857925
Median peak for Run 2 : 34.7358467514
Median peak for Run 3 : 34.7730805291
Median peak for Run 4 : 34.7956086453
Median peak for Run 5 : 34.8061017238
Median peak for Run 6 : 34.7342208662
Median peak for Run 7 : 34.78166738
Median peak for Run 8 : 34.920173919
Median peak for Run 9 : 34.8446582413
Median peak for Run 10 : 34.8085662213

Median static power : 16.0475573973
Median of all the median peaks (- static power) for dacapo:tradebeans : 18.7532977873