Max peak for Run 1 : 113.60837321536243
Max peak for Run 2 : 116.05818963847007
Max peak for Run 3 : 115.40561860144464
Max peak for Run 4 : 116.35819851351064
Max peak for Run 5 : 115.1388403534973
Max peak for Run 6 : 116.28596127573354
Max peak for Run 7 : 114.90708957469603
Max peak for Run 8 : 115.55396811081377
Max peak for Run 9 : 113.8120630716756
Max peak for Run 10 : 116.46437663221685

Median static power : 16.7843475566
Median of all the max peaks (- static power) for dacapo:sunflow : 98.6954457995