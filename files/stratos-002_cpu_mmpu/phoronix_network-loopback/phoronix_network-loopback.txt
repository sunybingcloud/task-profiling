Max peak for Run 1 : 32.20508852897631
Max peak for Run 2 : 32.149925976160446
Max peak for Run 3 : 29.505460114818764
Max peak for Run 4 : 31.91727607122087
Max peak for Run 5 : 31.181261389259483
Max peak for Run 6 : 29.55592172551225
Max peak for Run 7 : 30.837752846454038
Max peak for Run 8 : 30.663919967264405
Max peak for Run 9 : 30.6445509748857
Max peak for Run 10 : 30.820146066009535

Median static power : 16.7843475566
Median of all the max peaks (- static power) for phoronix:network-loopback : 14.0446018996