Max peak for Run 1 : 101.99372912104545
Max peak for Run 2 : 97.45692681676172
Max peak for Run 3 : 102.83680116081214
Max peak for Run 4 : 101.54340602072585
Max peak for Run 5 : 102.92001230387157
Max peak for Run 6 : 103.98051327197788
Max peak for Run 7 : 98.77336472814972
Max peak for Run 8 : 106.13730731160007
Max peak for Run 9 : 100.36351818677736
Max peak for Run 10 : 102.72259041902026

Median static power : 21.1913666072
Median of all the max peaks (- static power) for dacapo:pmd : 81.1667931628