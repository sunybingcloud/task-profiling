Max peak for Run 1 : 50.55082692895667
Max peak for Run 2 : 50.684637426348175
Max peak for Run 3 : 50.82088550103479
Max peak for Run 4 : 50.62556160112587
Max peak for Run 5 : 51.451014985306884
Max peak for Run 6 : 50.57325621076021
Max peak for Run 7 : 50.757729228705166
Max peak for Run 8 : 55.24600476534897
Max peak for Run 9 : 52.294502473038854
Max peak for Run 10 : 50.64927590630855

Median static power : 20.3233647132
Median of all the max peaks (- static power) for phoronix:audio-encoding : 30.3978186143