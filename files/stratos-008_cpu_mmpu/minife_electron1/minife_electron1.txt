Max peak for Run 1 : 77.45176204412243
Max peak for Run 2 : 77.2191545999588
Max peak for Run 3 : 78.02800092556583
Max peak for Run 4 : 77.05009082376468
Max peak for Run 5 : 78.2988819392284
Max peak for Run 6 : 79.07087160717603
Max peak for Run 7 : 77.05818020831654
Max peak for Run 8 : 77.61836628822935
Max peak for Run 9 : 78.82776126698498
Max peak for Run 10 : 78.86743091212679

Median static power : 20.3233647132
Median of all the max peaks (- static power) for minife:electron1 : 57.4998188937