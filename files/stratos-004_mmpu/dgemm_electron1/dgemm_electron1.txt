Max peak for Run 1 : 105.67549376029031
Max peak for Run 2 : 105.3285001090772
Max peak for Run 3 : 105.52054971594598
Max peak for Run 4 : 105.3964747764056
Max peak for Run 5 : 105.56246046894789
Max peak for Run 6 : 105.50142122514964
Max peak for Run 7 : 105.54005559275694
Max peak for Run 8 : 105.45495876781689
Max peak for Run 9 : 105.57768541552616
Max peak for Run 10 : 105.62702663094316

Median static power : 17.8023053223
Median of all the max peaks (- static power) for dgemm:electron1 : 87.7279973321