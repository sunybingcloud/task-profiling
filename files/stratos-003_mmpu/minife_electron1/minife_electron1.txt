Max peak for Run 1 : 98.77104539439479
Max peak for Run 2 : 99.67602418186911
Max peak for Run 3 : 100.6689641306149
Max peak for Run 4 : 98.86860991245018
Max peak for Run 5 : 99.81333555598812
Max peak for Run 6 : 99.31437480649865
Max peak for Run 7 : 98.99167869567148
Max peak for Run 8 : 94.34954529569066
Max peak for Run 9 : 99.56233331923978
Max peak for Run 10 : 91.87992888519727

Median static power : 18.2148536194
Median of all the max peaks (- static power) for minife:electron1 : 80.9381731317