Median peak for Run 1 : 55.69056619845866
Median peak for Run 7 : 55.976654907855675
Median peak for Run 5 : 56.56657921196171
Median peak for Run 9 : 56.01579619227955
Median peak for Run 6 : 56.76761068736913
Median peak for Run 8 : 56.33789036129718
Median peak for Run 3 : 56.9045373191142
Median peak for Run 2 : 56.381014164855586
Median peak for Run 4 : 55.84068083469954
Median peak for Run 10 : 56.62202259590035

Median static power : 17.78860212660441
Median of all the median peaks (- static power) for dacapo_eclipse : 38.57085013647197