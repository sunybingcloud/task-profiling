Median peak for Run 1 : 44.595346929436786
Median peak for Run 7 : 65.48799847661087
Median peak for Run 5 : 63.84169704719458
Median peak for Run 9 : 39.27145715146093
Median peak for Run 6 : 64.02435039055709
Median peak for Run 8 : 65.06234765766118
Median peak for Run 3 : 39.668043192722834
Median peak for Run 2 : 64.51140210352303
Median peak for Run 4 : 42.5932215853082
Median peak for Run 10 : 42.14896892204322

Median static power : 17.78860212660441
Median of all the median peaks (- static power) for dacapo_lusearch : 36.42991986171128