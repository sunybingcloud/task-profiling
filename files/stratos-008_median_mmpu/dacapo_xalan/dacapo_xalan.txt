Median peak for Run 1 : 95.8572602528953
Median peak for Run 7 : 96.57599546115298
Median peak for Run 5 : 96.42807510837306
Median peak for Run 9 : 100.60257432229433
Median peak for Run 6 : 86.03957733795582
Median peak for Run 8 : 94.75897587589839
Median peak for Run 3 : 97.15823673518909
Median peak for Run 2 : 93.32681389378966
Median peak for Run 4 : 95.53369371188897
Median peak for Run 10 : 104.21572090723424

Median static power : 20.323364713247166
Median of all the median peaks (- static power) for dacapo_xalan : 75.81930296738702