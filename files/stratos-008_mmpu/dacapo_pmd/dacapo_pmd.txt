Max peak for Run 1 : 102.74928684731735
Max peak for Run 2 : 103.07012706110696
Max peak for Run 3 : 103.91507829121383
Max peak for Run 4 : 99.50396013025987
Max peak for Run 5 : 102.13621495381508
Max peak for Run 6 : 100.07316633816576
Max peak for Run 7 : 102.67676777830022
Max peak for Run 8 : 101.69186661838786
Max peak for Run 9 : 99.07666303759045
Max peak for Run 10 : 102.27314520148933

Median static power : 23.4268487864
Median of all the max peaks (- static power) for dacapo:pmd : 78.7778312912