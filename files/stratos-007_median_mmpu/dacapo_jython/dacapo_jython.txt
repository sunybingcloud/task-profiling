Median peak for Run 1 : 49.0838785445157
Median peak for Run 7 : 48.632780000316096
Median peak for Run 5 : 48.134458607748854
Median peak for Run 9 : 48.840700631557496
Median peak for Run 6 : 48.16232425984065
Median peak for Run 8 : 50.50990464310837
Median peak for Run 3 : 48.69058112350339
Median peak for Run 2 : 48.56309226723458
Median peak for Run 4 : 48.83080755897158
Median peak for Run 10 : 49.32365832570358

Median static power : 15.872011904558398
Median of all the median peaks (- static power) for dacapo_jython : 32.88868243667908