Max peak for Run 1 : 38.457128908304036
Max peak for Run 2 : 36.87312462081131
Max peak for Run 3 : 34.78228213804285
Max peak for Run 4 : 37.70185698975786
Max peak for Run 5 : 36.888852151641856
Max peak for Run 6 : 34.99221891778544
Max peak for Run 7 : 36.91082949328399
Max peak for Run 8 : 36.851700612948974
Max peak for Run 9 : 36.17447643903387
Max peak for Run 10 : 36.4981905609977

Median static power : 19.7675111459
Median of all the max peaks (- static power) for phoronix:network-loopback : 17.0949014709