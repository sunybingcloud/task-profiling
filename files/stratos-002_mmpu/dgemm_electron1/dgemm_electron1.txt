Max peak for Run 1 : 107.24184965500956
Max peak for Run 2 : 106.93861351033138
Max peak for Run 3 : 107.14839441860047
Max peak for Run 4 : 107.25120896182
Max peak for Run 5 : 107.01468872840422
Max peak for Run 6 : 107.07914814174106
Max peak for Run 7 : 106.9962088022686
Max peak for Run 8 : 107.17234106247733
Max peak for Run 9 : 107.12148985858262
Max peak for Run 10 : 106.9581928246708

Median static power : 19.7675111459
Median of all the max peaks (- static power) for dgemm:electron1 : 87.3328078542