Max peak for Run 1 : 133.62830654521358
Max peak for Run 2 : 133.6594881576933
Max peak for Run 3 : 133.7693634033203
Max peak for Run 4 : 133.3953660735099
Max peak for Run 5 : 133.70927585096473
Max peak for Run 6 : 133.71308537324168
Max peak for Run 7 : 133.91908853304733
Max peak for Run 8 : 133.53709041494412
Max peak for Run 9 : 133.5562516095734
Max peak for Run 10 : 133.41170749672875

Median static power : 19.7675111459
Median of all the max peaks (- static power) for phoronix:cryptography : 113.876386206