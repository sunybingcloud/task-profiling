Max peak for Run 1 : 58.29864370590751
Max peak for Run 2 : 60.47319129825895
Max peak for Run 3 : 57.25342869729758
Max peak for Run 4 : 58.7223584070513
Max peak for Run 5 : 60.12871861234307
Max peak for Run 6 : 59.62114370567282
Max peak for Run 7 : 60.26594455645676
Max peak for Run 8 : 56.88540482801781
Max peak for Run 9 : 63.21571348564187
Max peak for Run 10 : 58.44526701317052

Median static power : 16.3206552218
Median of all the max peaks (- static power) for phoronix:audio-encoding : 42.8510958346