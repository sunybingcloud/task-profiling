Max peak for Run 1 : 85.77554015331762
Max peak for Run 2 : 94.16258613065537
Max peak for Run 3 : 89.45798713192787
Max peak for Run 4 : 86.9191226122547
Max peak for Run 5 : 85.6906807088356
Max peak for Run 6 : 87.30192638201197
Max peak for Run 7 : 86.2904848392154
Max peak for Run 8 : 88.38093027279736
Max peak for Run 9 : 81.6935960595971
Max peak for Run 10 : 89.47562508400763

Median static power : 26.0683502498
Median of all the max peaks (- static power) for dacapo:h2 : 61.0421742473