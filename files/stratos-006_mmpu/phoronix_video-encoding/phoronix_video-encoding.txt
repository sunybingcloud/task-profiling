Max peak for Run 1 : 135.7771650282431
Max peak for Run 2 : 150.52707531802193
Max peak for Run 3 : 149.18002834389478
Max peak for Run 4 : 147.21419698223679
Max peak for Run 5 : 144.90350563338188
Max peak for Run 6 : 150.98532668832738
Max peak for Run 7 : 150.68881731185482
Max peak for Run 8 : 143.03509613653878
Max peak for Run 9 : 150.12044982325986
Max peak for Run 10 : 148.25867792176362

Median static power : 26.0683502498
Median of all the max peaks (- static power) for phoronix:video-encoding : 122.651002883