Max peak for Run 1 : 118.90221418584511
Max peak for Run 2 : 114.17742024029582
Max peak for Run 3 : 120.47073360962723
Max peak for Run 4 : 119.31049265961349
Max peak for Run 5 : 120.64129922567821
Max peak for Run 6 : 121.13179330461169
Max peak for Run 7 : 115.09356620880052
Max peak for Run 8 : 124.64088924756366
Max peak for Run 9 : 117.80093759561655
Max peak for Run 10 : 120.55941520320833

Median static power : 26.0683502498
Median of all the max peaks (- static power) for dacapo:pmd : 93.8222628848