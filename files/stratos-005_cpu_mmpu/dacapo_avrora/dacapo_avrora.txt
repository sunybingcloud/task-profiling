Max peak for Run 1 : 85.13859576863423
Max peak for Run 2 : 88.18283893848559
Max peak for Run 3 : 85.98040329174279
Max peak for Run 4 : 82.43251197876292
Max peak for Run 5 : 81.05519293932826
Max peak for Run 6 : 78.88054083178379
Max peak for Run 7 : 82.9108424657085
Max peak for Run 8 : 81.58455360237207
Max peak for Run 9 : 81.2063608916616
Max peak for Run 10 : 83.75543124681431

Median static power : 22.1096378257
Median of all the max peaks (- static power) for dacapo:avrora : 60.5620393965