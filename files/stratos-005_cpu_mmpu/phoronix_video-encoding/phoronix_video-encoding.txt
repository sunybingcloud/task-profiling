Max peak for Run 1 : 143.3544036111664
Max peak for Run 2 : 146.02989008425874
Max peak for Run 3 : 135.49263197672482
Max peak for Run 4 : 143.65961409589113
Max peak for Run 5 : 140.24629549314315
Max peak for Run 6 : 144.4962292220695
Max peak for Run 7 : 143.74264796793716
Max peak for Run 8 : 144.23222930650343
Max peak for Run 9 : 144.9918847282005
Max peak for Run 10 : 140.23725930372626

Median static power : 22.1096378257
Median of all the max peaks (- static power) for phoronix:video-encoding : 121.591493206