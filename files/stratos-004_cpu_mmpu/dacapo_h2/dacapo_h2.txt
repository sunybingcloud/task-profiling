Max peak for Run 1 : 61.613084969839306
Max peak for Run 2 : 63.99925814368436
Max peak for Run 3 : 62.68710248893849
Max peak for Run 4 : 61.29556321499124
Max peak for Run 5 : 61.673113357197956
Max peak for Run 6 : 66.32793580546672
Max peak for Run 7 : 62.4127305490959
Max peak for Run 8 : 62.99163119518012
Max peak for Run 9 : 61.20313899142761
Max peak for Run 10 : 65.89567187704728

Median static power : 16.0475573973
Median of all the max peaks (- static power) for dacapo:h2 : 46.5023591218