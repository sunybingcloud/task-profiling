Max peak for Run 1 : 76.03283386258852
Max peak for Run 2 : 78.23381816113577
Max peak for Run 3 : 83.17880602884688
Max peak for Run 4 : 76.24667519116844
Max peak for Run 5 : 81.22096207552683
Max peak for Run 6 : 83.66152905427549
Max peak for Run 7 : 81.1936897453165
Max peak for Run 8 : 83.5000767278669
Max peak for Run 9 : 83.31560401349586
Max peak for Run 10 : 83.56789822203247

Median static power : 16.0475573973
Median of all the max peaks (- static power) for minife:electron1 : 66.1523266549