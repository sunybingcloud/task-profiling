'''
Pull the pcp data from all the stratos.
Given that the stratos-001 to stratos-008 are idle, this would mean that the pcp data pulled would correspond to the static power usage of those machines.
'''
#!usr/bin/python

import os
import sys

#create directory if not present
def create_dir(dirname):
	if not os.path.exists(dirname):
		os.makedirs(dirname)
	else:
		pass

#pulling static power from all the stratos
def pull_static_power(duration, dirname):
	create_dir(dirname)
	os.system("pmdumptext -t 1sec -s "+str(duration)+" -f '' -m -l -d , -c pcpconfigStaticPower > "+dirname+"/static_power.pcplog")

#printing usage error
def print_usage_error(message):
	print(message)
	print("USAGE: python3 pull_static_power_usage.py <duration> <destination directory>")

if __name__ == "__main__":
	args = sys.argv	
	if len(args) < 3:
		print_usage_error("Invalid number of arguments")
	else:
		pull_static_power(args[1], args[2])
