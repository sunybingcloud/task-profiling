import datetime
import random
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.patches as mpatches
import numpy as np
import sys
import csv
import operator
import os
destination_directory = ""

import numpy
def median(lst):
    return numpy.median(numpy.array(lst))


def make_directory():
    global destination_directory
    if not os.path.isdir(destination_directory):
        os.mkdir(destination_directory)

def calculate_static_power_node(static_power_log, node):
    static_power_fp = open(static_power_log,"r")
    static_power_lines = static_power_fp.readlines()
    static_cpu_power_dict = {}
    static_power_headers  =[]

    for i in static_power_lines[0].split(","):
        static_cpu_power_dict[i.strip()] = []
        static_power_headers.append(i.strip())
    #print(static_power_headers)
    for i in range(2,len(static_power_lines)):
        line = static_power_lines[i].split(",")
        for j in range(len(line)):
            if static_power_headers[j] not in static_cpu_power_dict:
                static_cpu_power_dict[static_power_headers[j]] = []
            static_cpu_power_dict[static_power_headers[j]].append(float(line[j].strip()))

    static_power_cpu_list = [] # list to append the list of cpu powers for a particular node eg : for stratos-001 : [[cpu-0 power list][cpu-6 power list]]
    static_power_dram_list = [] # list to append the list of dram powers for a particular node eg : for stratos-001 : [[cpu-0 power list][cpu-6 power list]]

    for i in static_cpu_power_dict:
        if node in i and "perfevent.hwcounters.rapl__RAPL_ENERGY_PKG.value" in i:
            static_power_cpu_list.append(static_cpu_power_dict[i])
        elif node in i and "perfevent.hwcounters.rapl__RAPL_ENERGY_DRAM.value" in i:
            static_power_dram_list.append(static_cpu_power_dict[i])

    static_power_cpu_list = [sum(x) for x in zip(*static_power_cpu_list)]
    median_static_power_cpu_node = median(static_power_cpu_list)
    median_static_power_cpu_node = median_static_power_cpu_node*(2**-32) # converting to watts
    print("static cpu power: ",median_static_power_cpu_node)

    static_power_dram_list = [sum(x) for x in zip(*static_power_dram_list)]
    median_static_power_dram_node = median(static_power_dram_list)
    median_static_power_dram_node = median_static_power_dram_node*(2**-32) # converting to watts
    print("static dram power: ",median_static_power_dram_node)

    # total_median_static_power_node = median_static_power_cpu_node + median_static_power_dram_node
    # changed by PK to just consider CPU power consumption of benchmarks.
    total_median_static_power_node = median_static_power_cpu_node 

    return total_median_static_power_node



# Have another function that computes median of median of peak power consumption.
def dump_median_peak_median(data, median_static_power_node):
  make_directory()
  line2 = ""
  for benchmark in data:
        sub_dir = destination_directory+"/"+benchmark
        if not os.path.isdir(sub_dir):
            os.mkdir(sub_dir)
        median_file = sub_dir+"/"+benchmark+".txt"
        fp = open(median_file,"w")
        median_peaks = []
        for run in data[benchmark].keys():
            median_peak_run = median(data[benchmark][run])
            median_peaks.append(median_peak_run)
            line = "Median peak for Run "+str(run)+" : "+str(median_peak_run)+"\n"
            fp.write(line)
        #print(median_static_power_node)
        median_peak = median(median_peaks)
        #print(benchmark,":",median_peak)
        median_peak -= median_static_power_node
        line = "\nMedian static power : "+str(median_static_power_node)
        fp.write(line)
        line = "\nMedian of all the median peaks (- static power) for "+benchmark+" : "+str(median_peak)
        line2 += "," + str(median_peak)
        fp.write(line)
        fp.close()
  fp = open(destination_directory+".csv", "w")
  fp.write(line2)

#MMPU
def dump_max_peak_median(data,median_static_power_node):
    make_directory()
    for benchmark in data:
        sub_dir = destination_directory+"/"+benchmark
        if not os.path.isdir(sub_dir):
            os.mkdir(sub_dir)
        median_file = sub_dir+"/"+benchmark+".txt"
        fp = open(median_file,"w")
        max_peaks = []
        for run in data[benchmark].keys():
            max_peak_run = max(data[benchmark][run])
            max_peaks.append(max_peak_run)
            line = "Max peak for Run "+str(run)+" : "+str(max_peak_run)+"\n"
            fp.write(line)
        #print(median_static_power_node)
        median_peak = median(max_peaks)
        #print(benchmark,":",median_peak)
        median_peak = median_peak - median_static_power_node
        line = "\nMedian static power : "+str(median_static_power_node)
        fp.write(line)
        line = "\nMedian of all the max peaks (- static power) for "+benchmark+" : "+str(median_peak)
        fp.write(line)
        fp.close()


if __name__ == "__main__":
    pcp_directory = sys.argv[1]
    baseline_run_power = {}
    destination_directory = sys.argv[2]
    #static_power_file = sys.argv[3]
    node = sys.argv[3]
    total_median_static_power_node = calculate_static_power_node("static_power.pcplog", node)
    print("total static power: ",total_median_static_power_node)


    for dirName, subdirList, fileList in os.walk(pcp_directory):
        #print('Found directory: %s' % dirName)
        for filename in fileList:
            directory = dirName.rsplit("/",1)[-1]
            file_path = os.path.join(dirName,filename)
            #print(file_path,"-->",directory)
            fp = open(file_path,"r")
            lines = fp.readlines()
            run_number = int(filename.split("_")[0])
            if directory not in baseline_run_power.keys():
                baseline_run_power[directory] = {}
            baseline_run_power[directory][run_number] = {}
            headers = lines[0].split(",")
            for i in headers:
                baseline_run_power[directory][run_number][i.strip()] = []
            for i in range(2,len(lines)):
                line = lines[i].split(",")
                for j in range(len(line)):
                    baseline_run_power[directory][run_number][headers[j].strip()].append(float(line[j]))

    cpu_power_dict = {}
    dram_power_dict = {}

    for i in baseline_run_power.keys():
        cpu_power_dict[i] = {}
        dram_power_dict[i] = {}
        for j in baseline_run_power[i].keys():
            cpu_power_dict[i][j] = []
            dram_power_dict[i][j] = []

    for i in baseline_run_power:
        for j in baseline_run_power[i]:
            for k in baseline_run_power[i][j]:
                if "RAPL_ENERGY_PKG" in k:
                    cpu_power_dict[i][j].append(baseline_run_power[i][j][k])
                elif "RAPL_ENERGY_DRAM" in k:
                    dram_power_dict[i][j].append(baseline_run_power[i][j][k])

    for i in cpu_power_dict:
        for j in cpu_power_dict[i]:
            power_list = [sum(x) for x in zip(*cpu_power_dict[i][j])]
            cpu_power_dict[i][j] = power_list
            cpu_power_dict[i][j] = [(x)*(2**-32) for x in power_list]

    for i in dram_power_dict:
        for j in dram_power_dict[i]:
            power_list = [sum(x) for x in zip(*dram_power_dict[i][j])]
            dram_power_dict[i][j] = power_list
            dram_power_dict[i][j] = [(x)*(2**-32) for x in power_list]

    total_power_dict = {}
    for benchmark in cpu_power_dict.keys():
        total_power_dict[benchmark] = {}
        for run_no in cpu_power_dict[benchmark].keys():
            # total_power_dict[benchmark][run_no] = [cpu_power + dram_power for cpu_power, dram_power in zip(cpu_power_dict[benchmark][run_no], dram_power_dict[benchmark][run_no])]
            total_power_dict[benchmark][run_no] = [cpu_power for cpu_power in cpu_power_dict[benchmark][run_no]]
            #print(total_power_dict[benchmark][run_no])
    dump_median_peak_median(total_power_dict,total_median_static_power_node)
