import datetime
import random
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.patches as mpatches
import numpy as np
import sys
import csv
import operator
import os
destination_directory = ""


def make_directory():
    global destination_directory
    destination_directory = sys.argv[len(sys.argv)-1]
    if not os.path.isdir(destination_directory):
        os.mkdir(destination_directory)

def plot_graph(data,ylabel,graph_name):
    #make_directory()
    '''for file in data:
        seconds = [sec for sec in range(0,len(data[file][node]))]
        plt.xlabel("Time (s)", fontsize=20)
        plt.ylabel(ylabel,fontsize=20)
        plt.plot(seconds,data[file][node],color="red", label= file)
        legend = plt.legend(fontsize=17,loc='upper center', bbox_to_anchor=(0.47, -0.10),fancybox=False, shadow=False, ncol=5)
        filename =  destination_directory+"/"+node+"_"+graph_name+"_"+file+".png"
        plt.savefig(filename, bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()'''
    make_directory()
    for benchmark in data:
        sub_dir = destination_directory+"/"+benchmark
        if not os.path.isdir(sub_dir):
            os.mkdir(sub_dir)
        for run in data[benchmark].keys():
            #print("Run : ",run,"\n",data[benchmark][run])
            seconds = [sec for sec in range(0,len(data[benchmark][run]))]
            plt.xlabel("Time (s)", fontsize=20)
            plt.ylabel(ylabel,fontsize=20)
            plt.plot(seconds,data[benchmark][run],color="red",label = str(run))
            legend = plt.legend(fontsize=17,loc='upper center', bbox_to_anchor=(0.47, -0.10),fancybox=False, shadow=False, ncol=5)
            filename =  sub_dir+"/"+graph_name+"_"+str(run)+".png"
            print("\nGenerating ",filename)
            plt.savefig(filename, bbox_extra_artists=(legend,), bbox_inches='tight')
            plt.close()

if __name__ == "__main__":
    pcp_directory = sys.argv[1]
    baseline_run_power = {}
    destination_directory = sys.argv[2]
    for dirName, subdirList, fileList in os.walk(pcp_directory):
        #print('Found directory: %s' % dirName)
        for filename in fileList:
            directory = dirName.rsplit("/",1)[-1]
            file_path = os.path.join(dirName,filename)
            #print(file_path,"-->",directory)
            fp = open(file_path,"r")
            lines = fp.readlines()
            run_number = int(filename.split("_")[0])
            if directory not in baseline_run_power.keys():
                baseline_run_power[directory] = {}
            baseline_run_power[directory][run_number] = {}
            headers = lines[0].split(",")
            for i in headers:
                baseline_run_power[directory][run_number][i.strip()] = []
            for i in range(2,len(lines)):
                line = lines[i].split(",")
                for j in range(len(line)):
                    baseline_run_power[directory][run_number][headers[j].strip()].append(float(line[j]))

    cpu_power_dict = {}
    dram_power_dict = {}

    for i in baseline_run_power.keys():
        cpu_power_dict[i] = {}
        dram_power_dict[i] = {}
        for j in baseline_run_power[i].keys():
            cpu_power_dict[i][j] = []
            dram_power_dict[i][j] = []

    for i in baseline_run_power:
        for j in baseline_run_power[i]:
            for k in baseline_run_power[i][j]:
                if "RAPL_ENERGY_PKG" in k:
                    cpu_power_dict[i][j].append(baseline_run_power[i][j][k])
                elif "RAPL_ENERGY_DRAM" in k:
                    dram_power_dict[i][j].append(baseline_run_power[i][j][k])

    for i in cpu_power_dict:
        for j in cpu_power_dict[i]:
            power_list = [sum(x) for x in zip(*cpu_power_dict[i][j])]
            cpu_power_dict[i][j] = power_list
            cpu_power_dict[i][j] = [(x)*(2**-32) for x in power_list]

    for i in dram_power_dict:
        for j in dram_power_dict[i]:
            power_list = [sum(x) for x in zip(*dram_power_dict[i][j])]
            dram_power_dict[i][j] = power_list
            dram_power_dict[i][j] = [(x)*(2**-32) for x in power_list]

    total_power_dict = {}
    for benchmark in cpu_power_dict.keys():
        total_power_dict[benchmark] = {}
        for run_no in cpu_power_dict[benchmark].keys():
            total_power_dict[benchmark][run_no] = [cpu_power + dram_power for cpu_power, dram_power in zip(cpu_power_dict[benchmark][run_no], dram_power_dict[benchmark][run_no])]

    plot_graph(total_power_dict,"Total Power (W)","Power")
